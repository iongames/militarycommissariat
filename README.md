# MilitaryCommissariat
Приложение для военного комиссариата предназначено для упрощения и автоматизации работы с военнообязанными.

Доступ к приложению может получить только сотрудник военного комиссариата с использованием Email и пароля. С помощью приложения можно добавлять, изменять и удалять данные о военнообязанных, а также использовать фильтры для удобного поиска.

<b>Техническое задание</b> - [Ссылка](https://gitlab.com/iongames/militarycommissariat/-/blob/main/%D0%A2%D0%B5%D1%85%D0%BD%D0%B8%D1%87%D0%B5%D1%81%D0%BA%D0%BE%D0%B5_%D0%B7%D0%B0%D0%B4%D0%B0%D0%BD%D0%B8%D0%B5.docx)
<b>Макет приложения в Figma</b> - [Ссылка](https://www.figma.com/file/j93vRzT68dI7IyZMfyedXW/Voenkomat?type=design&node-id=6%3A2&mode=design&t=eZi6bdKOVvkzO3oh-1)

## ER Диаграмма

![Диаграмма ER](https://gitlab.com/iongames/militarycommissariat/-/raw/main/%D0%94%D0%B8%D0%B0%D0%B3%D1%80%D0%B0%D0%BC%D0%BC%D0%B0.png)

• Military - таблица с информацией о военнообязанном.<br/>
• Passport - таблица с паспортными данными человека.<br/>
• MilitaryCommissariat - таблица с информацией о военном комиссариате.<br/>
• MilitaryDelay - таблица с информацией об отсрочке определенного военнообязанного.<br/>
• MilitaryCarLicense - связующая таблица с информацией о водительских правах военнообязанного.<br/>
• Employee - Таблица с информацией о сотрудниках военкомата.<br/>
